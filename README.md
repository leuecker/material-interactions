# Material Interactions

Use this project to calculate the interactions of a particle flight path with material.
The material can be described by mesh grid or functions.
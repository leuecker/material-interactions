from scipy.interpolate import interp2d
import numpy as np
from scipy.stats import multivariate_normal
import warnings

# f5, f6 from functions trigger these warning but in section that then evaluate to null anyway
warnings.filterwarnings('ignore', message='invalid value encountered in sqrt')
warnings.filterwarnings('ignore', message='No more knots can be added')
warnings.filterwarnings('ignore', message='A theoretically impossible result ')


class TF2:
    """
    minimal reconstruction of the ROOT TF2 class
    """
    def __init__(self, fnc):
        self.fnc = fnc

    def set_par(self, par):
        self.par = par.copy()

    def __call__(self, x, y):
        return np.where((x < self.xmax) & (x > self.xmin) & (y > self.ymin) & (y < self.ymax), self.fnc(x, y, self.par), np.nan)

    def update_par(self, index, new_value):
        self.par[index] = new_value

    def limits(self, xmin, xmax, ymin, ymax):
        self.xmin = xmin
        self.ymin = ymin
        self.xmax = xmax
        self.ymax = ymax

# create unity sphere for the uncertainty distance calculations
num_pts = 1000
indices = np.arange(0, num_pts, dtype=float) + 0.5

phi = np.arccos(1 - 2*indices/num_pts)
theta = np.pi * (1 + 5**0.5) * indices

x, y, z = np.cos(theta) * np.sin(phi), np.sin(theta) * np.sin(phi), np.cos(phi)
usphere = np.array([x, y, z]).T

def covs(c11, c22, c33, c12, c13, c23):
    res = np.empty((len(c11), 3, 3), dtype='float16')
    res[:, 0, 0] = c11
    res[:, 1, 1] = c22
    res[:, 2, 2] = c33
    res[:, 0, 1] = c12
    res[:, 1, 0] = c12
    res[:, 0, 2] = c13
    res[:, 2, 0] = c13
    res[:, 1, 2] = c23
    res[:, 2, 1] = c23
    return res


class ParModel:
    """
    Class to use when a fully parametrised model for the object is available.
    The usual restriction w.r.t. the boundary function apply, in some use cases it might be prudent
    to transform the coordinate system -> take care with the 'distance' method.
    """
    def __init__(self, f1, f2, par1, par2, name, x_min=-np.inf, x_max=np.inf, y_min=-np.inf, y_max=np.inf):
        """
        Primary constructor.
        param fx: function for the inner / outer boundary 2D-function, f1 should be more positiv
        param parx: parameters for fx
        param name: name of the model
        param x/y_min/max
        """
        self.name = str(name)
        self.type = 'parameterised'
        self.f1 = TF2(f1)
        self.f1.set_par(par1)
        self.f1.limits(x_min, x_max, y_min, y_max)
        self.f2 = TF2(f2)
        self.f2.set_par(par2)
        self.f2.limits(x_min, x_max, y_min, y_max)
        # check if the ordering of functions is correct:
        if (x_min != -np.inf) & (x_max != np.inf):
            x_test = (x_min + x_max)/2
        elif (x_min == -np.inf) & (x_max == np.inf):
            x_test = 0
        elif x_min == -np.inf:
            x_test = x_max - 1
        elif x_max == np.inf:
            x_test = x_min + 1
        if (y_min != -np.inf) & (y_max != np.inf):
            y_test = (y_min + y_max)/2
        elif (y_min == -np.inf) & (y_max == np.inf):
            y_test = 0
        elif y_min == -np.inf:
            y_test = y_max - 1
        elif y_max == np.inf:
            y_test = y_min + 1
        if self.f1(x_test, y_test) < self.f2(x_test, y_test):
            self.f1, self.f2 = self.f2, self.f1

    def inside(self, x, y):
        """
        Return True if an (x,y,z)-point falls within a module sensor.
        param [x,y,z]: position to evaluate
        """
        return (z > self.f2(x, y)) & (z <= self.f1(x, y))

    def inside2(self, locs, method='gen'):
        """
        Return if an x,y,z-point falls within a module sensor.
        param locs: np.array(N x 3[x,y,z]) positions to evaluate
        param method: 'gen' -> standard check of all point
                      'single' -> aborts as soon as a single point is True
        """
        if method == 'single':
            for loc in locs:
                if (loc[2] > self.f2(loc[0], loc[1])) & (loc[2] <= self.f1(loc[0], loc[1])):
                    return np.ones(locs.shape[0])
            return np.zeros(locs.shape[0])
        else:
            return (locs[:, 2] > self.f2(locs[:, 0], locs[:, 1])) & (locs[:, 2] <= self.f1(locs[:, 0], locs[:, 1]))

    def position(self, x, y, which=0):
        """
        Evaluate the boundary function.

        param x,y: arrays of the coordinates to be evaluated
        param which: 1, 2, which function to evaluate, if neither returns both
        """
        if which == 1:
            return self.f1(x, y)
        elif which == 2:
            return self.f2(x, y)
        else:
            return np.array([self.f1(x, y), self.f2(x, y)])

    def par(self, which=0):
        """
        return parameters for the boundary function.
        param which: 1, 2, which function to evaluate, if neither returns both
        """
        if which == 1:
            return self.f1.par
        elif which == 2:
            return self.f2.par
        else:
            return np.array([self.f1.par, self.f2.par])

    def prob(self, locs, covs, n1, n2, uth):
        """
        Return the probability that a vertex originates from the material

        param locs: array Nx3 3->[x,z,y] nominal locations
        param covs: array Nx3x3, covariance matrices
        param n1: number of random points generated in the initial prob calculation
        param n2: number of random points generated in the secondary prob calculation
        param uth: Upper threshold to suppress a secondary prob calculation as a fraction
        """
        res = np.zeros(locs.shape[0])
        for id in np.arange(len(res)):
            try:
                rv = multivariate_normal(mean=locs[id], cov=covs[id])
                one = rv.rvs(size=n1)
                prob = np.sum(self.inside2(one)) / n1
                if (prob < uth) & (prob != 0):
                    two = rv.rvs(size=n2)
                    prob = ((prob * n1) + np.sum(self.inside2(two))) / (n1 + n2)
                res[id] = prob
            except (ValueError, IndexError) as error:
                res[id] = 1
        return res

    def intersect(self, loc0s, loc1s, d=0.02):
        """
        Check if a particles flight path intersect with one of the sensors.

        param loc0: start point of flightpath
        param loc1: end point of flightpath
        param d: granularity
        """
        res = np.zeros(loc1s.shape[0])
        for id, loc0 in enumerate(loc0s):
            loc1 = loc1s[id]

            def f(l):
                return loc0 + np.outer(l, (loc1 - loc0))

            x_min = max(min(loc0[0], loc1[0]), self.f1.xmin)
            x_max = min(max(loc0[0], loc1[0]), self.f1.xmax)
            y_min = max(min(loc0[1], loc1[1]), self.f1.ymin)
            y_max = min(max(loc0[1], loc1[1]), self.f1.ymax)
            lmin = max(0, min((x_min - loc0[0]) / (loc1[0] - loc0[0]), (y_min - loc0[1]) / (loc1[1] - loc0[1])))
            lmax = min(1, max((x_max - loc0[0]) / (loc1[0] - loc0[0]), (y_max - loc0[0]) / (loc1[0] - loc0[0])))
            space = np.arange(lmin, lmax, d)
            res[id] = np.sum(self.inside2(f(space), method='single'))
        return (res > 0)

    def distance(self, locs, covs):
        """
        Calculate the Distance between vertex and VELO module in
        Probability space, in 1 sigma increments.

        Return value 100: dist larger 5 sigma
        Return value i = 1-5: the distance is in (i-1,i]
        Return value -1: there was an error in the calculation
        """
        res = np.zeros(locs.shape[0])
        for id in np.arange(len(locs)):
            try:
                val = 100
                sphere = np.matmul(usphere, covs[id])
                for sigma in np.arange(1, 6):
                    ssphere = (sphere * sigma) + locs[id]
                    if np.sum(self.inside2(ssphere, method='single')) != 0:
                        val = sigma
                        break
                res[id] = val
            except (ValueError, IndexError) as error:
                res[id] = -1
        return res


class GridModel:
    """
    Class to model an object with a grid setting the z-coordinate
    For computational reasons a rectangular, uniformly spaced grid is assumed to
    be shared by both the upper an lower boundary.
    """

    def __init__(self, xmin, xmax, ymin, ymax, g1, g2):
        """
        Constructor

        Generate X,Y grid
        Construct both halves

        param [x,y][min,max]: boundaries of the grid
        param g[1,2]: matching grid of the upper and lower boundary of the object.
                        where g1 is the more positive in z
        """
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax
        self.X, self.Y = np.meshgrid(np.linspace(xmin, xmax, g1.shape[0]),
                                     np.linspace(ymin, ymax, g1.shape[1]))
        self.g1 = g1
        self.g2 = g2
        if g1[1, 1] < g2[1, 1]:
            self.g1 = g2
            self.g2 = g1
        self.zmax = np.max(self.g1)
        self.zmin = np.min(self.g2)

    def get_idx(self, x):
        """
        return the index to obtain the next lower X grid value
        """
        return (np.floor((x - self.xmin)/(self.xmax - self.xmin) * self.g1.shape[0]) - 1).astype(int)

    def get_idy(self, y):
        """
        return the index to obtain the next lower Y grid value
        """
        return (np.floor((y - self.ymin) / (self.ymax - self.ymin) * self.g1.shape[1]) - 1).astype(int)

    def inside(self, locs, method='gen'):
        """
        Type: array of Bools

        Returns True if given point is inside the foil.

        param loc: Nx3 vector np.array([x,y,z])
        param method: 'gen' -> generous method, will determine the four grid points surrounding
                            the target and return True if the target is between the min and max.
                      'single' -> aborts as soon as a single check returns True
                      'interp' -> uses cubic interpolation
        """
        if method == 'interp':
            res = np.empty(locs.shape[0])
            for id, loc in enumerate(locs):
                if ((loc[0] < self.xmin) | (loc[0] > self.xmax) | (loc[1] < self.ymin) |
                        (loc[1] > self.ymax) | (loc[2] > self.zmax) | (loc[2] < self.zmin)):
                    res[id] = False
                else:
                    idx = self.get_idx(loc[0])
                    idy = self.get_idy(loc[1])
                    f1 = interp2d(x=self.X[idx - 1:idx + 3, idy - 1:idy + 3],
                                  y=self.Y[idx - 1:idx + 3, idy - 1:idy + 3],
                                  z=self.g1[idx - 1:idx + 3, idy - 1:idy + 3],
                                  kind='cubic')
                    f2 = interp2d(x=self.X[idx - 1:idx + 3, idy - 1:idy + 3],
                                  y=self.Y[idx - 1:idx + 3, idy - 1:idy + 3],
                                  z=self.g2[idx - 1:idx + 3, idy - 1:idy + 3],
                                  kind='cubic')
                    res[id] = (f1(loc[0], loc[1]) > loc[2]) & (loc[2] > f2(loc[0], loc[1]))
        elif method == 'single':
            res = np.empty(locs.shape[0])
            for id, loc in enumerate(locs):
                if ((loc[0] < self.xmin) | (loc[0] > self.xmax) | (loc[1] < self.ymin) |
                        (loc[1] > self.ymax) | (loc[2] > self.zmax) | (loc[2] < self.zmin)):
                    res[id] = False
                else:
                    idx = self.get_idx(loc[0])
                    idy = self.get_idy(loc[1])
                    test1 = max(self.g1[idx: idx + 2, idy: idy + 2])
                    test2 = min(self.g2[idx: idx + 2, idy: idy + 2])
                    if (test2 < loc[2]) & (test1 > loc[2]):
                        return np.ones(len(res))
            res = np.zeros(len(res))
        else:
            res = np.empty(locs.shape[0])
            for id, loc in enumerate(locs):
                if ((loc[0] < self.xmin) | (loc[0] > self.xmax) | (loc[1] < self.ymin) |
                        (loc[1] > self.ymax) | (loc[2] > self.zmax) | (loc[2] < self.zmin)):
                    res[id] = False
                else:
                    idx = self.get_idx(loc[0])
                    idy = self.get_idy(loc[1])
                    test1 = max(self.g1[idx: idx + 2, idy: idy + 2])
                    test2 = min(self.g2[idx: idx + 2, idy: idy + 2])
                    res[id] = ((test2 < loc[2]) & (test1 > loc[2]))
        return res

    def probability(self, locs, covs, n1=1000, n2=10000, uth=0.05):
        """
        Type: array-floats

        Returns the probability that the given point is located within the foil.

        param loc: np.array([x,y,z]) coordinates of position to be checked.
        param cov: 3x3 Covariance matrix associated to the vertex point.
        param half: 0 for both, +/-1 for left/right half only.

        param n1: number of random points generated in the initial prob calculation
        param n2: number of random points generated in the secondary prob calculation
        param uth: Upper threshold to suppress a secondary prob calculation as a fraction of n1
        """
        prob = np.zeros(locs.shape[0])
        for i in np.arange(len(prob)):
            rv = multivariate_normal(mean=locs[i, :], cov=covs[i, :, :])
            sample = rv.rvs(size=n1)
            one = self.inside(sample)
            if (np.sum(one) != 0) & (np.sum(one) < uth*n1):
                sample = rv.rvs(size=n2)
                two = self.inside(sample)
                return (np.sum(one) + np.sum(two)) / (n1 + n2)
            else:
                return np.sum(one) / n1

    def Material(self, loc0s, loc1s):
        """
        Calculates the amount of material a particle has passed through in the module
        """
        res = np.zeros(loc0s.shape[0])
        for id in np.arange(len(res)):
            loc0 = loc0s[id, :]
            loc1 = loc1s[id, :]
            length = np.sqrt(np.sum((loc1 - loc0) ** 2))

            def f(t):
                return loc0 + np.outer(t, (loc1 - loc0)) / length

            ints = self.intersects(f, length)
            res[id] = self.material(ints, f)
        return res

    def material(self, ints, f, stepsize=0.002):
        if isinstance(ints, int):
            return 0
        mat = 0
        for inter in ints:
            t = np.arange(inter[0], inter[1] + stepsize, stepsize)
            locs = f(t)
            mat += np.sum(self.inside(locs, 'interp')) / (len(t) + 1) * (inter[1] + stepsize - inter[0])
        return mat

    def intersects(self, f, length, delta=0.1):
        """
        Type: np array:

        Returns a list of arrays containing:
            fmin - a FD shortly before entry into foil
            fmax - a FD shortly after exit from foil
            angle - angle of approach relative to foil

        param delta: stepsize for the coarse scan
        """
        t = np.arange(0, length + delta, delta)
        locs = f(t)
        tmp = self.inside(locs)
        tmin = t[:-1][(1 * tmp[1:] - 1 * tmp[:-1]) == 1]
        tmax = t[1:][(1 * tmp[1:] - 1 * tmp[:-1]) == -1]
        ints = np.empty((len(tmin), 3))
        ints[:, 0] = tmin
        ints[:, 1] = tmax
        return ints

    def intersect(self, loc0s, loc1s, d=0.02):
        """
        Check if a particles flight path intersect with one of the sensors.

        param loc0: start point of flightpath
        param loc1: end point of flightpath
        param d: granularity
        """
        res = np.zeros(loc1s.shape[0])
        for id, loc0 in enumerate(loc0s):
            loc1 = loc1s[id]

            def f(l):
                return loc0 + np.outer(l, (loc1 - loc0))

            x_min = max(min(loc0[0], loc1[0]), self.xmin)
            x_max = min(max(loc0[0], loc1[0]), self.xmax)
            y_min = max(min(loc0[1], loc1[1]), self.ymin)
            y_max = min(max(loc0[1], loc1[1]), self.ymax)
            lmin = max(0, min((x_min - loc0[0]) / (loc1[0] - loc0[0]), (y_min - loc0[1]) / (loc1[1] - loc0[1])))
            lmax = min(1, max((x_max - loc0[0]) / (loc1[0] - loc0[0]), (y_max - loc0[0]) / (loc1[0] - loc0[0])))
            space = np.arange(lmin, lmax, d)
            res[id] = np.sum(self.inside(f(space), method='single'))
        return res > 0

    def distance(self, locs, covs):
        """
        Calculate the Distance between vertex and VELO module in
        Probability space, in 1 sigma increments.

        Return value 100: dist larger 5 sigma
        Return value i = 1-5: the distance is in (i-1,i]
        Return value -1: there was an error in the calculation
        """
        res = np.zeros(locs.shape[0])
        for id in np.arange(len(locs)):
            try:
                val = 100
                sphere = np.matmul(usphere, covs[id])
                for sigma in np.arange(1, 6):
                    ssphere = (sphere * sigma) + locs[id]
                    if np.sum(self.inside(ssphere, method='single')) != 0:
                        val = sigma
                        break
                res[id] = val
            except (ValueError, IndexError) as error:
                res[id] = -1
        return res


class Combination:
    """
    Class to combine calculations on multiple objects
    """

    def __init__(self, models):
        """
        Default Constructor

        param models: array of model to be included
        """
        self.models = models

    def intersect(self, loc0s, loc1s, d=0.02):
        """
        Type Bool array

        Return True if the flight path intersects one of the parts

        param loc0s: starting point of the flight path
        param loc1s: end point of the flight paths
        """
        res = np.zeros(loc0s.shape[0])
        for mod in self.models:
            res += mod.intersect(loc0s, loc1s, d)
        return res > 0

    def distance(self, locs, covs):
        """
        Type Bool array

        Return True if the flight path intersects one of the parts

        param loc0s: starting point of the flight path
        param loc1s: end point of the flight paths
        """
        res = np.zeros((locs.shape[0], len(self.models)))
        for id, mod in enumerate(self.models):
            res[:, id] += mod.distance(locs, covs)
        return np.min(res, axis=1)

    def prob(self, locs, covs):
        """
        Type: array of floats.

        Returns the probability of of a vertex actually being in material.

        param locs: Nx3 vector of the vertex positions.
        param covs: covariance matrix of said vertices.
        """
        res = np.zeros(locs.shape[0])
        for mod in self.models:
            res += mod.prob(locs, covs)
        return np.where(res > 1, 1, res)
